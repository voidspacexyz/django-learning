This repo contains my django learning material.

It is organized in a way that the students would learn multiple things apart from just django.
1. Django
2. git
3. Some basic TUI.

## How to use this repo.

There are basically 9 steps in this repo. 

To shift between each step, clone this repo (git clone git@gitlab.com:voidspacexyz/django-learning.git)  and execute the following command.

## cd django-learning

## git checkout step1  or git checkout step8 etc.


PS : Please make sure git is installed in your computer, if not, 

Fedora : sudo dnf install git  or Debian : sudo apt-get install git.


Once you are inside a step[1-9], it would contain a file called "what" (yes that is the actual file name). That file explain what we are going to do in each step, and what files you should refer to.

** the what files are WIP, should have cleaned it out soon. If you have already cleaned it out, do send me a MR (Merge Request)

Once you refer to the what file, open the files mentioned in it and go ahead and start learning. The code that you refer will have comments for major code snippets, incase if you dont understand some code, feel free to learn on your own and edit the code and send a MR again. I will happy to merge things like that.


## Good luck.

If you are stuck with something, there are 3 ways to get help.
1. Raise an issue in the repo.
2. Write an email to me. Mail ID : null@voidspace.xyz  (Yes, that is my email, I am spamming you)
3. This is possibly the fastest way, go figure out yourself. There are irc channels, SO (StackOverflow), there is something called a search engine use it, find local people who know django and pair program with them. Trust me, that is possibly the fastest way to learn.

Anyways, kudos to anybody using this repo.
